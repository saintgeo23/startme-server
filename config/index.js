module.exports = {
  port: process.env.PORT || 3000,
  originDev: 'http://127.0.0.1:8080',
  originProd: 'https://startme-test.surge.sh',
};
