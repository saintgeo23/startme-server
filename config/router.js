const Router = require('koa-router');
const request = require('request-promise');

const router = new Router();

router.get('/:id', async (ctx, next) => {
  if (ctx.params.id === undefined) {
    return ctx.status = 404;
  }

  try {
    const data = await request.get(`https://start.me/p/${ctx.params.id}.json`);

    ctx.status = 200;
    return ctx.body = data;
  } catch (error) {
    console.log(`error: ${error}`);
    return ctx.status = 500;
  }
});

module.exports = router;
