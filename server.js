const Koa = require('koa');
const app = new Koa();
const config = require('./config');
const router = require('./config/router.js');
const cors = require('koa-cors');

const origin = process.env.NODE_ENV === 'production' ? config.originProd : config.originDev;

app.use(cors({
  origin,
}));

app.use(router.routes());

app.listen(config.port, () => {
  console.log(`Server listens port ${config.port}`);
});
